-- Creacion base datos

create database proyecto collate='utf8mb4_spanish_ci';
alter database proyecto character set = "utf8mb4";

-- Seleccion base datos
use proyecto;

-- creacion tablas 
create table usuario(
id int unsigned auto_increment primary key,
nombre varchar(30) not null,
apellidos varchar(60) not null,
contrasenha varchar(40) not null,
foto longblob,
confirmacion boolean default 1,
email varchar(60) unique not null,
fecha timestamp default current_timestamp
);

create table categoria(
id int unsigned auto_increment primary key,
titulo varchar(60) not null
);

create table articulo(
id int unsigned auto_increment primary key,
id_usuario int unsigned not null,
id_categoria int unsigned not null,
titulo varchar(60) not null,
descripcion longtext not null,
localidad varchar(400) not null,
precio decimal(8,2) not null,
confirmacionVenta boolean default 0,
valoracion longtext,
fecha timestamp default current_timestamp,
constraint articulo_usuarioid_fk foreign key (id_usuario)
	references usuario(id) on delete cascade,
constraint articulo_categoriaid_fk foreign key (id_categoria)
	references categoria(id) on delete cascade 
);
 
create table compra(
id_articulo int unsigned not null,
id_usuario int unsigned not null,
fecha timestamp default current_timestamp,
primary key(id_articulo, id_usuario),
constraint compra_articuloid_fk foreign key (id_articulo)
	references articulo(id) on delete cascade,
constraint compra_usuarioid_fk foreign key (id_usuario)
	references usuario(id) on delete cascade
);

create table fotos_articulo(
id int unsigned auto_increment primary key,
fotos longblob,
id_articulo int unsigned not null,
constraint fotoarticulo_articuloid_fk foreign key (id_articulo)
	references articulo(id) on delete cascade
);